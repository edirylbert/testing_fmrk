package com.bdd.step;

import com.bdd.generic.Service;
import com.bdd.page.AutomationpracticePage;
import com.bdd.page.ContactUsAutomationPractice.ContactUsAutomationPracticePage;
import com.bdd.page.LoginAutomationPractice.LoginAutomationPracticePage;
import com.bdd.page.RegisterAutomationPractice.RegisterAutomationPracticePage;
import cucumber.api.DataTable;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
public class AutomationpracticeStep extends ScenarioSteps {

    private AutomationpracticePage automationpracticePage;
    private RegisterAutomationPracticePage registerAutomationPractice;
    private LoginAutomationPracticePage loginAutomationPractice;
    private ContactUsAutomationPracticePage contactUsAutomationPracticePage;
    private Service service;


    @Step("Se carga el driver de chrome y la pagina de Automationpractice")
    public void cargarLaPaginaDeAutomationPractice() {
        automationpracticePage.open();
        getDriver().manage().window().maximize();
    }

    @Step("Doy click a Sign In en Automationpractice")
    public void doyClickASignInAutomationpractice() {
        automationpracticePage.doyClickASignInAutomationpractice();
    }

    @Step("Escribo mi el correo que deseo regitrar")
    public void escriboMiCorreo(String correo) {
        automationpracticePage.escribirCorreo(correo);
    }

    @Step("Doy click en boton Create an account")
    public void  doyClickAlBotonCreateAnAccount() {
        registerAutomationPractice.doyClickAlBotonCreateAnAccount();
    }

    @Step("Ingreso datos al formulario")
    public void ingresoDatosFormulario(DataTable dataTable) {

        registerAutomationPractice.ingresoDatosFormulario(dataTable);
    }
    @Step("Doy click en boton Register")
    public void doyClickAlBotonRegister() {
        registerAutomationPractice.doyClickAlBotonRegister();
    }
    @Step("Valido mensaje de Create an account")
    public boolean validoMensajeDeConfirmacion() {
        return registerAutomationPractice.validoMensajeDeConfirmacion();
    }
    @Step("Valido mensaje de error")
    public boolean validoElMensajeDeError(String mensajeError) {
        return  registerAutomationPractice.validoElMensajeDeError(mensajeError);
    }

    @Step("Ingreso usuario contrasenia")
    public void ingresoUsuarioContrasenia(String correo, String password) {
        loginAutomationPractice.ingresoUsuarioContrasenia(correo,password);
    }
    @Step("Doy click en Sign in Login")
    public void doyClickEnSignInLogin() {
        loginAutomationPractice.doyClickEnSignInLogin();
    }
    @Step("Validacion de mensaje erroneo cuenta no existe")
    public boolean validoElMensajeDeErrorLogin(String mensajeError) {
        return loginAutomationPractice.validoElMensajeDeErrorLogin(mensajeError);
    }

    @Step("Doy click en CONTACT US")
    public void doyClickEnCONTACTUS() {
        contactUsAutomationPracticePage.doyClickEnCONTACTUS();
    }
    @Step("Ingreso datos formulario CONTACT US")
    public void ingresoDatosFormularioCONTACTUS(DataTable dataTable) {
        contactUsAutomationPracticePage.ingresoDatosFormularioCONTACTUS(dataTable);
    }

    @Step("doy click en SEND")
    public void doyClickEnSEND() {
        contactUsAutomationPracticePage.doyClickEnSEND();
    }

    @Step("Valido formulario CONTACT US enivado correctamente")
    public boolean validoFormularioCONTACTUSEnivadoCorrectamente(String mensajeSuccess) {
        return contactUsAutomationPracticePage.validoFormularioCONTACTUSEnivadoCorrectamente(mensajeSuccess);
    }

    @Step("Ingreso datos  erroneos select formulario CONTACT US")
    public void ingresoDatosErroneosFormularioSelectCONTACTUS(String correErronero,String mensaje) {
        contactUsAutomationPracticePage.ingresoDatosErroneosFormularioSelectCONTACTUS(correErronero, mensaje);
    }
    @Step("Valido mensaje error Select incorrecto")
    public boolean validoMensajeErrorSelectIncorrecto(String msjCorreoIncorrecto) {
        return contactUsAutomationPracticePage.validoMensajeErrorSelectIncorrecto(msjCorreoIncorrecto);
    }
    @Step("Valido mensaje error Correo incorrecto")
    public void ingresoDatosErroneosFormularioTextareaCONTACTUS(String correo, String mensaje) {

    }
}
