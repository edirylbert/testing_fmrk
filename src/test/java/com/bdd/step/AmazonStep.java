package com.bdd.step;

import com.bdd.page.AmazonPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class AmazonStep extends ScenarioSteps {

    private AmazonPage amazonPage;

    @Step("Cargo la pagina de Amazon")
    public void cargarLaPaginaDeAmazon() {
        amazonPage.open();
        getDriver().manage().window().maximize();
    }

    @Step("Escribiendo el producto en Amazon")
    public void escribirProductoAmazon(String producto) {
        amazonPage.escribirProductoAmazon(producto);
    }

    @Step("Dando click en el boton de búsqueda")
    public void hagoClickEnBusqueda() {
        amazonPage.hagoClickEnBusqueda();
    }

    @Step("Dando click en la opcion del rango de precios de cincuenta a cien dólares")
    public void doyClickEnLaOpcionDeDolaresCincuentaACien() {
        amazonPage.doyClickEnLaOpcionDeDolaresEnElRangoDeCincuentaACien();
    }

    @Step("Validando resultados de la búsqueda")
    public String validarResultados() {
        return amazonPage.validarResultados();
    }
}
