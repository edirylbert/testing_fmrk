package com.bdd.step;

import com.bdd.page.EbayPage;
import com.bdd.page.LoginEbay.LoginEbayPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class EbayStep extends ScenarioSteps {

    private EbayPage ebayPage;
    private LoginEbayPage loginEbayPage;

    @Step("Se carga el driver de chrome y la pagina de Ebay")
    public void cargarLaPaginaDeEbay() {
        ebayPage.open();
        getDriver().manage().window().maximize();
    }

    @Step("Se escribe el producto en ebay")
    public void escriboElProductoEnEbay(String producto) {
        ebayPage.escriboElProductoEnEbay(producto);
    }

    @Step("Dando click en la busqueda de ebay")
    public void doyClickEnLaBusquedaDeEbay() {
        ebayPage.doyClickEnLaBusquedaDeEbay();
    }

    @Step("Dando click en la marca de adidas en Ebay")
    public void doyClickEnLaMarcaAdidasEnEbay() {
        ebayPage.doyClickEnLaMarcaAdidasEnEbay();
    }

    @Step("Valido el resultado de la busqueda en Ebay")
    public boolean validoResultadoDeLaBusquedaEnEbay() {
        return ebayPage.validoResultadoDeLaBusquedaEnEbay();
    }

    @Step("Doy click a iniciar sesion en ebay")
    public void doyClickAIniciarSesionEnEbay() {
        ebayPage.doyClickAIniciarSesionEnEbay();
    }

    @Step("Escribiendo mi usuario")
    public void escriboMiUsuario(String usuario) {
        loginEbayPage.escriboMiUsuario(usuario);
    }

    @Step("Dando click al boton continuar")
    public void doyClickAlBotonContinuar() {
        loginEbayPage.doyClickAlBotonContinuar();
    }

    @Step("Validando que el mensaje de error sea el esperado")
    public boolean validoMensajeDeErrorEbay(String mensajeError) {
        return loginEbayPage.validoMensajeDeErrorEbay(mensajeError);
    }
}
