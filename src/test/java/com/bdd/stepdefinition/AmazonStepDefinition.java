package com.bdd.stepdefinition;

import com.bdd.step.AmazonStep;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class AmazonStepDefinition {
    @Steps
    private AmazonStep amazonStep;

    @Dado("^que cargo la pagina de Amazon$")
    public void cargarLaPaginaDeAmazon() {
        amazonStep.cargarLaPaginaDeAmazon();
    }

    @Cuando("^escribo el producto \"([^\"]*)\"$")
    public void escriboElProducto(String producto) {
        amazonStep.escribirProductoAmazon(producto);
    }

    @Y("^hago click en la busqueda$")
    public void hagoClickEnLaBusqueda() {
        amazonStep.hagoClickEnBusqueda();
    }

    @Y("^doy click en la opcion de dolares cincuenta a cien$")
    public void doyClickEnLaOpcionDeDolaresCincuentaACien() {
        amazonStep.doyClickEnLaOpcionDeDolaresCincuentaACien();
    }

    @Entonces("^valido los resultados$")
    public void validoLosResultados() {
        Assert.assertEquals("1 a 24 de 509 resultados para" , amazonStep.validarResultados());
    }
}
