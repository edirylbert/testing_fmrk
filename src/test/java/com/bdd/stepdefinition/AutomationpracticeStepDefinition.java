package com.bdd.stepdefinition;

import com.bdd.step.AutomationpracticeStep;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
public class AutomationpracticeStepDefinition {

    @Steps
    private AutomationpracticeStep automationpracticeStep;

    @Dado("^Cargar la pagina de automationpractice$")
    public void cargarLaPaginaDeAutomationpractice() {
        automationpracticeStep.cargarLaPaginaDeAutomationPractice();
    }

    @Cuando("^doy click a Sign in  en automationpractice$")
    public void doyClickASignInAutomationpractice() {automationpracticeStep.doyClickASignInAutomationpractice();}

    @Y("^escribo mi correo \"([^\"]*)\"$")
    public void escriboMiCorreo(String usuario) {
        automationpracticeStep.escriboMiCorreo(usuario);
    }

    @Y("^doy click al boton Create an account$")
    public void doyClickAlBotonCreateAnAccount() { automationpracticeStep.doyClickAlBotonCreateAnAccount(); }


    @Entonces("^Ingreso datos formulario$")
    public void ingresoDatosFormulario(DataTable dataTable) {
        automationpracticeStep.ingresoDatosFormulario(dataTable);

    }

    @Y("^doy click al boton Register$")
    public void doyClickAlBotonRegister() { automationpracticeStep.doyClickAlBotonRegister(); }

    @Entonces("^Valido mensaje de confirmacion$")
    public void validoMensajeDeConfirmacion() {
        Assert.assertTrue("No se encontró correctamente el mensaje de error.",
                automationpracticeStep.validoMensajeDeConfirmacion());
    }

    @Entonces("^Valido mensaje de error \"([^\"]*)\"$")
    public void validoElMensajeDeError(String mensajeError) {
        Assert.assertTrue("No se encontró correctamente el mensaje de error.",
                automationpracticeStep.validoElMensajeDeError(mensajeError));
    }


    @Entonces("^ingreso usuario contrasenia  \"([^\"]*)\" \"([^\"]*)\"$")
    public void ingresoUsuarioContrasenia(String correo, String password)  {
        automationpracticeStep.ingresoUsuarioContrasenia(correo,password);
    }

    @Y("^doy click en Sign in Login$")
    public void doyClickEnSignInLogin() {
        automationpracticeStep.doyClickEnSignInLogin();
    }

    @Entonces("^Valido mensaje de error login \"([^\"]*)\"$")
    public void validoMensajeDeErrorLogin(String mensajeError)  {
        Assert.assertTrue("No se encontró correctamente el mensaje de error.",
                automationpracticeStep.validoElMensajeDeErrorLogin(mensajeError));
    }

    @Y("^doy click en CONTACT US$")
    public void doyClickEnCONTACTUS() {
        automationpracticeStep.doyClickEnCONTACTUS();
    }

    @Entonces("^Ingreso datos formulario CONTACT US$")
    public void ingresoDatosFormularioCONTACTUS(DataTable dataTable) {
        automationpracticeStep.ingresoDatosFormularioCONTACTUS(dataTable);

    }

    @Y("^doy click en SEND$")
    public void doyClickEnSEND() {
        automationpracticeStep.doyClickEnSEND();
    }


    @Y("^valido formulario CONTACT US enivado correctamente \"([^\"]*)\"$")
    public void validoFormularioCONTACTUSEnivadoCorrectamente(String mensajeSuccess)  {
        Assert.assertTrue("No se encontró correctamente el mensaje de error.",
                automationpracticeStep.validoFormularioCONTACTUSEnivadoCorrectamente(mensajeSuccess));
    }

    @Entonces("^Ingreso datos  erroneos formulario select CONTACT US \"([^\"]*)\" \"([^\"]*)\"$")
    public void ingresoDatosErroneosFormularioSelectCONTACTUS(String correo, String mensaje)  {
      automationpracticeStep.ingresoDatosErroneosFormularioSelectCONTACTUS(correo,mensaje);
    }
    @Entonces("^Ingreso datos  erroneos formulario textarea CONTACT US \"([^\"]*)\" \"([^\"]*)\"$")
    public void ingresoDatosErroneosFormularioTextareaCONTACTUS(String correo, String mensaje)  {
        automationpracticeStep.ingresoDatosErroneosFormularioTextareaCONTACTUS(correo,mensaje);
    }

    @Entonces("^valido mensaje error \"([^\"]*)\"$")
    public void validoMensajeError(String msjCorreoIncorrecto) throws Throwable {
        Assert.assertTrue("No se encontró correctamente el mensaje de error.",
                automationpracticeStep.validoMensajeErrorSelectIncorrecto(msjCorreoIncorrecto));
    }
}
