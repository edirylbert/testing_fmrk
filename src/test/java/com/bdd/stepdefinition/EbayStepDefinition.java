package com.bdd.stepdefinition;

import com.bdd.page.LoginEbay.LoginEbayPage;
import com.bdd.step.EbayStep;
import cucumber.api.PendingException;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class EbayStepDefinition {
    @Steps
    private EbayStep ebayStep;
    private LoginEbayPage loginEbayPage;

    @Dado("^Cargar la pagina de Ebay$")
    public void cargarLaPaginaDeEbay() {
        ebayStep.cargarLaPaginaDeEbay();
    }

    @Cuando("^Escribo el producto \"([^\"]*)\" en Ebay$")
    public void escriboElProductoEnEbay(String producto) {
        ebayStep.escriboElProductoEnEbay(producto);
    }

    @Y("^doy click en la busqueda de Ebay$")
    public void doyClickEnLaBusquedaDeEbay() {
        ebayStep.doyClickEnLaBusquedaDeEbay();
    }

    @Y("^doy click en la marca Adidas en Ebay$")
    public void doyClickEnLaMarcaAdidasEnEbay() {
        ebayStep.doyClickEnLaMarcaAdidasEnEbay();
    }

    @Entonces("^valido el resultado de la busqueda en Ebay$")
    public void validoElResultadoDeLaBusquedaEnEbay() {
        Assert.assertTrue("No se encontró el resultado esperado" ,
                ebayStep.validoResultadoDeLaBusquedaEnEbay());
    }

    @Cuando("^doy click a iniciar sesion en ebay$")
    public void doyClickAIniciarSesionEnEbay() {
        ebayStep.doyClickAIniciarSesionEnEbay();
    }

    @Y("^escribo mi \"([^\"]*)\"$")
    public void escriboMi(String usuario) {
        ebayStep.escriboMiUsuario(usuario);
    }

    @Y("^doy click al boton continuar$")
    public void doyClickAlBotonContinuar() {
        ebayStep.doyClickAlBotonContinuar();
    }

    @Entonces("^valido el mensaje \"([^\"]*)\" de ebay$")
    public void validoElMensajeDeEbay(String mensajeError) {
        Assert.assertTrue("No se encontró correctamente el mensaje de error.",
                ebayStep.validoMensajeDeErrorEbay(mensajeError));
    }
}
