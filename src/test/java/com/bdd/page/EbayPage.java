package com.bdd.page;


import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.ebay.com/")
public class EbayPage extends PageObject {

    @FindBy(xpath = "//input[@id='gh-ac']")
    private WebElementFacade txtBusqueda;
    @FindBy(xpath = "//input[@id='gh-btn']")
    private WebElementFacade busquedaButton;
    @FindBy(xpath = "//input[@aria-label='adidas']//parent::span//parent::div")
    private WebElementFacade marcaButton;

    @FindBy(xpath = "//li//div[@tabindex=0]//div[@id='s0-14-11-0-1-2-6-0-2[1]-4[0]-3']")
    private WebElementFacade tallaCalzadosSlide;
    @FindBy(xpath = "//li//div[@tabindex=0]//div[@id='s0-14-11-0-1-2-6-0-2[1]-4[1]-3']")
    private WebElementFacade colorSlide;

    @FindBy(xpath = "//h1[@class='srp-controls__count-heading']")
    private WebElementFacade mensajeResultados;

    @FindBy(xpath = "//span[@id='gh-ug']//a[contains(text(), 'Inicia sesión')]")
    private WebElementFacade iniciaSesionButton;


    public void escriboElProductoEnEbay(String producto) {
        txtBusqueda.sendKeys(producto);
    }

    public void doyClickEnLaBusquedaDeEbay() {
        clickOn(busquedaButton);
    }

    public void doyClickEnLaMarcaAdidasEnEbay() {

        WebElement colorSlide2 = (WebElement) getDriver().findElement(By.xpath("//li//div[@tabindex=0]//div[@id='s0-14-11-0-1-2-6-0-2[1]-4[1]-3']"));

        tallaCalzadosSlide.click();
        colorSlide2.click();
        marcaButton.click();
    }

    public boolean validoResultadoDeLaBusquedaEnEbay() {
        System.out.println("Texto del elemento es: " + mensajeResultados.getText());
        return mensajeResultados.isVisible();
    }

    public void doyClickAIniciarSesionEnEbay() {
        iniciaSesionButton.click();
    }

}
