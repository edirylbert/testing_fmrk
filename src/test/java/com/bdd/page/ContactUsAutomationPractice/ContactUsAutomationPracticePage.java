package com.bdd.page.ContactUsAutomationPractice;

import com.bdd.generic.Service;
import cucumber.api.DataTable;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.jruby.RubyProcess;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ContactUsAutomationPracticePage extends PageObject {

    private Service service = new Service();

    @FindBy(xpath = "//div[@id='contact-link']")
    private WebElementFacade contactLink;

    @FindBy(xpath = "//input[@id='email']")
    private WebElementFacade emailInput;

    @FindBy(xpath = "//textarea[@id='message']")
    private WebElementFacade messageTextarea;

    @FindBy(xpath = "//button[@id='submitMessage']")
    private WebElementFacade submitMessage ;

    @FindBy(xpath = "//div[@id='center_column']/p")
    private WebElementFacade msjCorrecto ;

    @FindBy(xpath = "//div[@id='center_column']/div/ol/li")
    private WebElementFacade msjIncorrecto ;



    public void doyClickEnCONTACTUS() {
        contactLink.click();
    }

    public void ingresoDatosFormularioCONTACTUS(DataTable dataTable) {
        String SubjectHeading = this.service.getValueFromDataTable(dataTable, "SubjectHeading");
        String xpathSubjectHeading = "//select[@id='id_contact']/option[contains(text(),'"+SubjectHeading+"')]";
        System.out.println(xpathSubjectHeading);
        WebElement SubjectHeadingSelect = (WebElement) getDriver().findElement(By.xpath(xpathSubjectHeading));
        SubjectHeadingSelect.click();

        emailInput.sendKeys(this.service.getValueFromDataTable(dataTable, "Emailaddress"));
        messageTextarea.sendKeys(this.service.getValueFromDataTable(dataTable, "Message"));

    }
    public void doyClickEnSEND() {
        submitMessage.click();
    }

    public boolean validoFormularioCONTACTUSEnivadoCorrectamente(String mensajeSuccess) {

        return mensajeSuccess.equals(msjCorrecto.getText());
    }

    public void ingresoDatosErroneosFormularioSelectCONTACTUS(String correErronero,String mensaje) {
        messageTextarea.sendKeys(mensaje);
        emailInput.sendKeys(correErronero);
    }

    public boolean validoMensajeErrorSelectIncorrecto(String msj) {
        System.out.println("mensjae -->"+   msjIncorrecto.getText());
        return msj.equals(msjIncorrecto.getText());
    }
}
