package com.bdd.page.RegisterAutomationPractice;

import com.bdd.generic.Service;
import cucumber.api.DataTable;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterAutomationPracticePage extends PageObject {

    private Service service = new Service();

    @FindBy(xpath = "//button[@id='SubmitCreate']")
    private WebElementFacade createAnAccountButton;


    @FindBy(xpath = "//input[@id='customer_firstname']")
    private WebElementFacade firstnameInput;

    @FindBy(xpath = "//input[@id='customer_lastname']")
    private WebElementFacade lastnameInput;

    @FindBy(xpath = "//input[@id='email']")
    private WebElementFacade emailInput;

    @FindBy(xpath = "//input[@id='passwd']")
    private WebElementFacade passwdInput;

    @FindBy(xpath = "//input[@id='firstname']")
    private WebElementFacade firstnameAddressInput;

    @FindBy(xpath = "//input[@id='lastname']")
    private WebElementFacade lastnameAddressInput;

    @FindBy(xpath = "//input[@id='company']")
    private WebElementFacade companyAddressInput;

    @FindBy(xpath = "//input[@id='address1']")
    private WebElementFacade address1AddressInput;

    @FindBy(xpath = "//input[@id='city']")
    private WebElementFacade cityAddressInput;

    @FindBy(xpath = "//input[@id='postcode']")
    private WebElementFacade postcodeAddressInput;



    @FindBy(xpath = "//select[@id='id_state']/option[2]")
    private WebElementFacade stateAddressInput;


    @FindBy(xpath = "//select[@id='id_country']/option[@value=21]")
    private WebElementFacade countryAddressSelect;

    @FindBy(xpath = "//input[@id='phone']")
    private WebElementFacade phoneAddressInput;


    @FindBy(xpath = "//button[@id='submitAccount']")
    private WebElementFacade registerButton;

    @FindBy(xpath = "//div[@id='center_column']/h1")
    private WebElementFacade msjValidacion;

    @FindBy(xpath = "//div[@id='create_account_error']/ol/li")
    private WebElementFacade msjError;



    public void doyClickAlBotonCreateAnAccount() {
        createAnAccountButton.click();
    }

    public void ingresoDatosFormulario(DataTable dataTable) {
        String titleForm =  this.service.getValueFromDataTable(dataTable, "Title");
        String xpathRadio ="";
        if (titleForm.equals("Mr")){
             xpathRadio = "//div[@id='uniform-id_gender1']";
        }
        else {
             xpathRadio =  "//div[@id='uniform-id_gender2']";
        }

        WebElement radioMrMrs = (WebElement) getDriver().findElement(By.xpath(xpathRadio));
        radioMrMrs.click();
        firstnameInput.sendKeys( this.service.getValueFromDataTable(dataTable, "Firstname"));
        lastnameInput.sendKeys( this.service.getValueFromDataTable(dataTable, "Lastname"));
        System.out.println(this.service.getValueFromDataTable(dataTable, "Email"));
        emailInput.sendKeys( this.service.getValueFromDataTable(dataTable, "Email"));
        passwdInput.sendKeys( this.service.getValueFromDataTable(dataTable, "Password"));

        String formatFecha = this.service.getValueFromDataTable(dataTable, "DateofBirth");
        String[] fecha = formatFecha.split("/");
        String dia = fecha[0];

        String xpathDia = "//select[@id='days']/option[@value="+dia.toString()+"]";
        WebElement diaForm = (WebElement) getDriver().findElement(By.xpath(xpathDia));
        diaForm.click();

        String mes = fecha[1];

        String xpathMes = "//select[@id='months']/option[@value="+mes.toString()+"]";
        System.out.println(xpathMes);
        WebElement mesForm = (WebElement) getDriver().findElement(By.xpath(xpathMes));
        mesForm.click();


        String anio = fecha[2];
        String xpathAnio = "//select[@id='years']/option[@value="+anio+"]";
        WebElement anioForm = (WebElement) getDriver().findElement(By.xpath(xpathAnio));
        anioForm.click();

        firstnameAddressInput.sendKeys( this.service.getValueFromDataTable(dataTable, "FirstnameAddress"));
        lastnameAddressInput.sendKeys( this.service.getValueFromDataTable(dataTable, "LastnameAddress"));
        companyAddressInput.sendKeys( this.service.getValueFromDataTable(dataTable, "Company"));
        address1AddressInput.sendKeys( this.service.getValueFromDataTable(dataTable, "Address"));
        cityAddressInput.sendKeys( this.service.getValueFromDataTable(dataTable, "City"));
        postcodeAddressInput.sendKeys( this.service.getValueFromDataTable(dataTable, "Code"));
        stateAddressInput.click();
        countryAddressSelect.click();

        phoneAddressInput.sendKeys( this.service.getValueFromDataTable(dataTable, "phone"));

    }

    public void doyClickAlBotonRegister() {
        registerButton.click();
    }

    public boolean validoMensajeDeConfirmacion() {
        System.out.println(msjValidacion.getText());

        return msjValidacion.getText().equals("MY ACCOUNT");
    }


    public boolean validoElMensajeDeError(String mensajeError) {

        return mensajeError.equals(msjError.getText());
    }
}
