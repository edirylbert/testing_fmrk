package com.bdd.page;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


@DefaultUrl("http://automationpractice.com/index.php")
public class AutomationpracticePage extends PageObject {


    @FindBy(xpath = "//a[contains(text(), 'Sign in')]")
    private WebElementFacade SignInLink;


    @FindBy(xpath = "//input[@id='email_create']")
    private WebElementFacade correoInput;

    @FindBy(xpath = "//button[@id='SubmitCreate']")
    private WebElementFacade createAnAccountButton;

    public void doyClickASignInAutomationpractice() {
        SignInLink.click();
    }

    public void escribirCorreo(String correo) {
        correoInput.sendKeys(correo);
    }


}
