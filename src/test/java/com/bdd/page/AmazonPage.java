package com.bdd.page;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.amazon.com/")

public class AmazonPage extends PageObject {
    @FindBy(xpath = "//input[@id='twotabsearchtextbox']") //definición del xpath
    private WebElementFacade inputBusqueda; //asignar el xpath a la variable

    @FindBy(xpath = "//span[@id='nav-search-submit-text']")
    private WebElementFacade buttonBusqueda;

    @FindBy(xpath = "//span[contains(text(), '$50 a $100')]")
    //@FindBy(xpath = "//[@id='p_36 1253504011']/span/a/span")
    private WebElementFacade rangoDePrecioStatico;

    @FindBy(xpath = "//span[contains(text(), 'resultados para')]")
    private WebElementFacade mensajeResultadoBusqueda;

    public void escribirProductoAmazon(String producto) {
        //ejecutar la acción de escribir
        inputBusqueda.sendKeys(producto);
    }

    public void hagoClickEnBusqueda() {
        buttonBusqueda.click();
    }

    public void doyClickEnLaOpcionDeDolaresEnElRangoDeCincuentaACien() {
        rangoDePrecioStatico.click();
    }

    public String validarResultados() {
        return mensajeResultadoBusqueda.getText();
    }
}
