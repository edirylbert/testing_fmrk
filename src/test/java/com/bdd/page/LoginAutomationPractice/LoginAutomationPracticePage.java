package com.bdd.page.LoginAutomationPractice;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;

public class LoginAutomationPracticePage extends PageObject {

    @FindBy(xpath = "//input[@id='email']")
    private WebElementFacade emailInput;

    @FindBy(xpath = "//input[@id='passwd']")
    private WebElementFacade passwdInput;

    @FindBy(xpath = "//button[@id='SubmitLogin']")
    private WebElementFacade submitLogin;

    @FindBy(xpath = "//div[@id='center_column']/div[1]/ol/li")
    private WebElementFacade msjError;

    public void ingresoUsuarioContrasenia(String correo, String password) {
        emailInput.sendKeys(correo);
        passwdInput.sendKeys(password);
    }

    public void doyClickEnSignInLogin() {
        submitLogin.click();
    }

    public boolean validoElMensajeDeErrorLogin(String mensajeError) {
        return mensajeError.equals(msjError.getText());
    }
}
