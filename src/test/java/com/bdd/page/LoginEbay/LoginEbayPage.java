package com.bdd.page.LoginEbay;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;

/**
 *  Flujo: Login, capacitación de everis pantalla.
 *  Número de página: 1
 *  version 1.0
**/


public class LoginEbayPage extends PageObject {


    @FindBy(xpath = "//input[@id='userid']")
    private WebElementFacade nombreUsuarioInput;

    @FindBy(xpath = "//button[@id='signin-continue-btn']")
    private WebElementFacade signInContinueButton;

    @FindBy(xpath = "//p[@id='signin-error-msg']")
    private WebElementFacade mensajeErrorTxt;

    public void escriboMiUsuario(String usuario) {
        nombreUsuarioInput.sendKeys(usuario);
    }

    public void doyClickAlBotonContinuar() {
        signInContinueButton.click();
    }

    public boolean validoMensajeDeErrorEbay(String mensajeError) {
        return mensajeErrorTxt.getText().equals(mensajeError);
    }
}
