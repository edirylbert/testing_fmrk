package com.bdd.generic;

import cucumber.api.DataTable;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;
import java.util.Map;

public class Service extends ScenarioSteps {
    public Service() {
    }
    public  String getValueFromDataTable(DataTable dataTable, String title) {
        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        return (String)((Map)list.get(0)).get(title);
    }
}
