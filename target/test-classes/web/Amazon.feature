#language:es
@TEST_AMAZON
Característica: Validar la busqueda en Amazon
  Como un usuario
  Quiero validar la pagina de Amazon
  Para realizar una busqueda

  Antecedentes: Abriendo la pagina de amazon
    Dado que cargo la pagina de Amazon

  @AMAZON_Ejemplo1_Estatico
  Esquema del escenario: Busqueda en amazon
    Cuando escribo el producto "<producto>"
    Y hago click en la busqueda
    Y doy click en la opcion de dolares cincuenta a cien
    Entonces valido los resultados
    Ejemplos:
    | producto |
    | Alexa |
    | Iphone |
    #Intentar realizar la busqueda