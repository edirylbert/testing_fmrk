#language: es
@Automation_Practice
Característica: Validacion de Registro de usuario en  automationpractice
  Como un usuario comun
  Quiero rehistrarme en la pagina automationpractice

  Antecedentes: Iniciar la pagina de automationpractice
    Dado Cargar la pagina de automationpractice

  #ESCENARIO CON HAPPY PATH
  @Automation_Practice_Register
  Esquema del escenario: Accediendo al Home de automationpractice para registrar usuario
    Cuando doy click a Sign in  en automationpractice
    Y escribo mi correo "<correo>"
    Y doy click al boton Create an account
    Entonces Ingreso datos formulario
      | Title |Firstname|Lastname |Email               |Password|DateofBirth|FirstnameAddress |LastnameAddress|Company    |Address             |City       |Code |phone    |
      | Mr    |edir     | quispe  |                    |123456  |29/1/1995  |DateofBirth      |DateofBirth    |DateofBirth|47 ANYVILLE RD NW #2|DateofBirth|99999|953683752|
    Y doy click al boton Register
    Entonces Valido mensaje de confirmacion
    Ejemplos:
      |        correo              |
      | gervacio6everis@gmail.com  |


      #ESCENARIO CON UNHAPPY PATH
  @Automation_Practice_Register_Failed
  Esquema del escenario: Accediendo al Home de automationpractice para registrar usuario
    Cuando doy click a Sign in  en automationpractice
    Y escribo mi correo "<correo>"
    Y doy click al boton Create an account

    Entonces Valido mensaje de error "<error>"
    Ejemplos:
      |        correo              |  error|
      | gervacio5everis@gmail.com  |   An account using this email address has already been registered. Please enter a valid password or request a new one.|

 #ESCENARIO CON HAPPY PATH
  @Automation_Practice_Login
  Esquema del escenario: Accediendo al Home de automationpractice para ingresar
    a una cuenta previamente registrada
    Cuando doy click a Sign in  en automationpractice
    Entonces ingreso usuario contrasenia  "<correo>" "<contrasenia>"
    Y doy click en Sign in Login
    Entonces Valido mensaje de confirmacion
    Ejemplos:
      |        correo              | contrasenia|
      | gervacio4everis@gmail.com  | 123456     |

    #ESCENARIO CON UNHAPPY PATH
  @Automation_Practice_Login_Failed
  Esquema del escenario: Accediendo al Home de automationpractice para ingresar
  a una cuenta previamente registrada
    Cuando doy click a Sign in  en automationpractice
    Entonces ingreso usuario contrasenia  "<correo>" "<contrasenia>"
    Y doy click en Sign in Login
    Entonces Valido mensaje de error login "<error>"
    Ejemplos:
      |        correo              | contrasenia    | error                 |
      | edirylbert@casa.com        |    123456      | Authentication failed.|


    #ESCENARIO CON HAPPY PATH
  @Automation_Practice_CONTACT_US
  Esquema del escenario: Accediendo al Home de automationpractice para ingresar
  a una cuenta previamente registrada y validar el formulario CONTACT US
    Cuando doy click a Sign in  en automationpractice
    Entonces ingreso usuario contrasenia  "<correo>" "<contrasenia>"
    Y doy click en Sign in Login
    Entonces Valido mensaje de confirmacion
    Y doy click en CONTACT US
    Entonces Ingreso datos formulario CONTACT US
      | SubjectHeading  |Emailaddress                  |Orderreference|Message|
      | Webmaster       |                              | webmaster    |hola mundo|
    Y doy click en SEND
    Y valido formulario CONTACT US enivado correctamente "<msjSuccess>"
    Ejemplos:
      |        correo              | contrasenia|msjSuccess|
      | gervacio4everis@gmail.com  | 123456     | Your message has been successfully sent to our team.|



    #ESCENARIO CON UNHAPPY PATH
  @Automation_Practice_CONTACT_US_Failed_Select
  Esquema del escenario: Accediendo al Home de automationpractice para ingresar
  a una cuenta previamente registrada y validar el formulario CONTACT US
    Cuando doy click a Sign in  en automationpractice
    Entonces ingreso usuario contrasenia  "<correo>" "<contrasenia>"
    Y doy click en Sign in Login
    Entonces Valido mensaje de confirmacion
    Y doy click en CONTACT US
    Entonces Ingreso datos  erroneos formulario select CONTACT US "<correErronero>" "<mensaje>"
    Y doy click en SEND

    Entonces valido mensaje error "<msjSlectInavlido>"

    Ejemplos:
      |        correo              | contrasenia|   correErronero| msjSlectInavlido                               |mensaje     |
      | gervacio4everis@gmail.com  | 123456     |    asdas       | Please select a subject from the list provided.| hola       |



    #ESCENARIO CON UNHAPPY PATH
  @Automation_Practice_CONTACT_US_Failed_TextArea
  Esquema del escenario: Accediendo al Home de automationpractice para ingresar
  a una cuenta previamente registrada y validar el formulario CONTACT US
    Cuando doy click a Sign in  en automationpractice
    Entonces ingreso usuario contrasenia  "<correo>" "<contrasenia>"
    Y doy click en Sign in Login
    Entonces Valido mensaje de confirmacion
    Y doy click en CONTACT US
    Entonces Ingreso datos  erroneos formulario textarea CONTACT US "<correErronero>" "<mensaje>"
    Y doy click en SEND

    Entonces valido mensaje error "<msjTextareaInavlido>"

    Ejemplos:
      |        correo              | contrasenia|   correErronero| msjTextareaInavlido             |mensaje|
      | gervacio4everis@gmail.com  | 123456     |    asdas       | The message cannot be blank. |       |