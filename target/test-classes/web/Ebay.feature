#language: es
  @TEST_EBAY
  Característica: Validacion de busqueda en Ebay
    Como un usuario comun
    Quiero validar la pagina de Ebay y hacer una busqueda
    Para mostrar los resultados de la busqueda realizada

  Antecedentes: Iniciar la pagina de Ebay
    Dado Cargar la pagina de Ebay

  #ESCENARIO CON HAPPY PATH
  @EBAY_Ejemplo1
  Esquema del escenario: Accediendo al Home de Ebay para validar la busqueda
    Cuando Escribo el producto "<producto>" en Ebay
    Y doy click en la busqueda de Ebay
    Y doy click en la marca Adidas en Ebay
    Entonces valido el resultado de la busqueda en Ebay
    Ejemplos:
    | producto |
    | zapatos  |

  #ESCENARIO DISTINTO CON UNHAPPY PATH
  @EBAY_EJEMPLO2_LOGIN
  Esquema del escenario: Accediendo a Ebay e intentando Loguearme con credenciales incorrectas
    Cuando doy click a iniciar sesion en ebay
    Y escribo mi "<usuario>"
    Y doy click al boton continuar
    Entonces valido el mensaje "<mensajeError>" de ebay
    Ejemplos:
      | usuario | mensajeError |
      |  | La información no coincide. |