$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/web/Automationpractice.feature");
formatter.feature({
  "name": "Validacion de Registro de usuario en  automationpractice",
  "description": "  Como un usuario comun\n  Quiero rehistrarme en la pagina automationpractice",
  "keyword": "Característica",
  "tags": [
    {
      "name": "@Automation_Practice"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Accediendo al Home de automationpractice para registrar usuario",
  "description": "",
  "keyword": "Esquema del escenario",
  "tags": [
    {
      "name": "@Automation_Practice_Register"
    }
  ]
});
formatter.step({
  "name": "doy click a Sign in  en automationpractice",
  "keyword": "Cuando "
});
formatter.step({
  "name": "escribo mi correo \"\u003ccorreo\u003e\"",
  "keyword": "Y "
});
formatter.step({
  "name": "doy click al boton Create an account",
  "keyword": "Y "
});
formatter.step({
  "name": "Ingreso datos formulario",
  "keyword": "Entonces ",
  "rows": [
    {
      "cells": [
        "Title",
        "Firstname",
        "Lastname",
        "Email",
        "Password",
        "DateofBirth",
        "FirstnameAddress",
        "LastnameAddress",
        "Company",
        "Address",
        "City",
        "Code",
        "phone"
      ]
    },
    {
      "cells": [
        "Mr",
        "edir",
        "quispe",
        "",
        "123456",
        "29/1/1995",
        "DateofBirth",
        "DateofBirth",
        "DateofBirth",
        "47 ANYVILLE RD NW #2",
        "DateofBirth",
        "99999",
        "953683752"
      ]
    }
  ]
});
formatter.step({
  "name": "doy click al boton Register",
  "keyword": "Y "
});
formatter.step({
  "name": "Valido mensaje de confirmacion",
  "keyword": "Entonces "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Ejemplos",
  "rows": [
    {
      "cells": [
        "correo"
      ]
    },
    {
      "cells": [
        "gervacio6everis@gmail.com"
      ]
    }
  ]
});
formatter.background({
  "name": "Iniciar la pagina de automationpractice",
  "description": "",
  "keyword": "Antecedentes"
});
formatter.step({
  "name": "Cargar la pagina de automationpractice",
  "keyword": "Dado "
});
formatter.match({
  "location": "AutomationpracticeStepDefinition.cargarLaPaginaDeAutomationpractice()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Accediendo al Home de automationpractice para registrar usuario",
  "description": "",
  "keyword": "Esquema del escenario",
  "tags": [
    {
      "name": "@Automation_Practice"
    },
    {
      "name": "@Automation_Practice_Register"
    }
  ]
});
formatter.step({
  "name": "doy click a Sign in  en automationpractice",
  "keyword": "Cuando "
});
formatter.match({
  "location": "AutomationpracticeStepDefinition.doyClickASignInAutomationpractice()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "escribo mi correo \"gervacio6everis@gmail.com\"",
  "keyword": "Y "
});
formatter.match({
  "location": "AutomationpracticeStepDefinition.escriboMiCorreo(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "doy click al boton Create an account",
  "keyword": "Y "
});
formatter.match({
  "location": "AutomationpracticeStepDefinition.doyClickAlBotonCreateAnAccount()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ingreso datos formulario",
  "rows": [
    {
      "cells": [
        "Title",
        "Firstname",
        "Lastname",
        "Email",
        "Password",
        "DateofBirth",
        "FirstnameAddress",
        "LastnameAddress",
        "Company",
        "Address",
        "City",
        "Code",
        "phone"
      ]
    },
    {
      "cells": [
        "Mr",
        "edir",
        "quispe",
        "",
        "123456",
        "29/1/1995",
        "DateofBirth",
        "DateofBirth",
        "DateofBirth",
        "47 ANYVILLE RD NW #2",
        "DateofBirth",
        "99999",
        "953683752"
      ]
    }
  ],
  "keyword": "Entonces "
});
formatter.match({
  "location": "AutomationpracticeStepDefinition.ingresoDatosFormulario(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "doy click al boton Register",
  "keyword": "Y "
});
formatter.match({
  "location": "AutomationpracticeStepDefinition.doyClickAlBotonRegister()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Valido mensaje de confirmacion",
  "keyword": "Entonces "
});
formatter.match({
  "location": "AutomationpracticeStepDefinition.validoMensajeDeConfirmacion()"
});
formatter.result({
  "status": "passed"
});
});